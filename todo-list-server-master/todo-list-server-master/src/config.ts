const config = {
    serverPort: 4000,
    serverAddress: 'localhost'
};

export default config;