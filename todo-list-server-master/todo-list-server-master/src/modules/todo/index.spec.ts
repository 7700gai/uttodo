import {ITodo, ITodoWithoutCompleted} from './interfaces';
import TodoService from './todo.service';
import {IDB} from "../../db/interfaces";
import { ITodoList } from "modules/todo-list/interfaces";



let todoService: TodoService;
let dbFake: IDB<ITodo>;
let listId: string;

const data = new Date()

const test: ITodoWithoutCompleted = {
    
    listId: 'First list',
    task: 'First task',
    finishDate: data
}

const updatedTask = (id:string, task:string, date:Date, isCompleted: boolean): ITodo => {
    return{
        listId: listId,
        task: task,
        finishDate: date,
        isCompleted: isCompleted
    }
}

describe('add task',() => {
    beforeEach(() => {
        // jest.resetAllMocks();
        dbFake = {
            get: jest.fn(),
            getAll: jest.fn(),
            add: jest.fn(),
            edit: jest.fn(),
            remove: jest.fn()
        };
        todoService = new TodoService(dbFake);
    });
    
    it('add new task', () => {

        todoService.add(test);

        expect(todoService.add).toHaveBeenCalledTimes(1);
        expect(todoService.add).toBeCalledWith(test);
        expect(dbFake.add).toHaveBeenCalledTimes(1);
        expect(dbFake.add).toBeCalledWith(test);
    });

    it('edit task', () => {

       const editTask = updatedTask("theListUp","theTaskUp",data,false);

       const response = todoService.edit("missionId", editTask);

        expect(todoService.edit).toHaveBeenCalledTimes(1);
        expect(todoService.edit).toBeCalledWith("missionId", editTask);
        expect(dbFake.edit).toHaveBeenCalledTimes(1);
        expect(dbFake.edit).toBeCalledWith("missionId",editTask);
        expect(response).toEqual({ id: "missionId",listId: listId,
         task: "theTaskUp",finishDate: data, isCompleted: false});
    });

    it('edit finishDate of task', () => {

        const editTask = updatedTask("theListUp","theTaskUp",data,false);

        const response = todoService.editDate("missionId", new Date());

        expect(todoService.editDate).toHaveBeenCalledTimes(1);
        expect(todoService.editDate).toBeCalledWith("missionId", editTask);
        expect(dbFake.edit).toHaveBeenCalledTimes(1);
        expect(dbFake.edit).toBeCalledWith("missionId",editTask );
        expect(response).toEqual({ id: "missionId",listId: listId, 
        task: "theTaskUp",finishDate: new Date(), isCompleted: false});
    });

    it('Mark a task as completed', () => {

        const editTask = updatedTask("1","sleep",new Date(),true);
        
        todoService.toggle("missionId");

        expect(todoService.toggle).toHaveBeenCalledTimes(1);
        expect(todoService.toggle).toBeCalledWith("missionId");
        expect(dbFake.edit).toHaveBeenCalledTimes(1);
        expect(dbFake.edit).toBeCalledWith("missionId", editTask);
    });

    it('get task by id', () => {

        todoService.getByID("missionId");

        expect(todoService.getByID).toHaveBeenCalledTimes(1);
        expect(todoService.getByID).toBeCalledWith("missionId");
        expect(dbFake.get).toHaveBeenCalledTimes(1);
        expect(dbFake.get).toBeCalledWith("missionId");

    });

    it('get all tasks', () => {

        todoService.getAll();

        expect(todoService.getAll).toHaveBeenCalledTimes(1);
        expect(dbFake.getAll).toHaveBeenCalledTimes(1);
    });

    it('delete task', () => {

        todoService.remove("missionId");

        expect(todoService.remove).toHaveBeenCalledTimes(1);
        expect(todoService.remove).toBeCalledWith("missionId");
        expect(dbFake.remove).toHaveBeenCalledTimes(1);
        expect(dbFake.remove).toBeCalledWith("missionId");
    });
})

